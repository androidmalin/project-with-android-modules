package ru.rma.apps.project.with.android.modules.module002

import ru.rma.apps.project.with.android.modules.module003.AndroidModule003

class AndroidModule002 {
    fun module002() {
        println("module002")
        AndroidModule003().module003()
    }
}