#!/bin/sh
files=`find . -name "*.gradle.kts" -type f`

function renameEachApk(){
    cur=`pwd`
    oldName=$1
    oldName2=`echo ${oldName:1}`
    newName=`echo $oldName2 | sed 's/.kts//g'`
    from="$cur$oldName2"
    to="$cur$newName"
    echo "$from  ==>  $to"
    mv "$cur$oldName2" "$cur$newName"
}

function renameApk() {
    #0.list file
    files=`find . -name "*.gradle.kts" -type f`

    #1.rename each apk
    for file in $files
    do
       if [[ $file == *.kts ]]; then
         renameEachApk "$file"
       fi
    done
}
renameApk